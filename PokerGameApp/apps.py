from django.apps import AppConfig


class PokergameappConfig(AppConfig):
    name = 'PokerGameApp'
