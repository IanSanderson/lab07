from django.db import models


# Create your models here.
class User(models.Model):
    Username = models.CharField(max_length=20)
    Highscore = models.IntegerField(default=0)

    def getHighScore(self):
        return self.Highscore

    def getUsername(self):
        return self.Username
