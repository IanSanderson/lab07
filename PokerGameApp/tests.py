from django.test import TestCase
from PokerGameApp.models import User


class TestHighScore(TestCase):
    def test_01(self):
        User.objects.create(Username="g", Highscore=0)
        testUser = User.objects.get(Username="g")

        self.assertEqual(testUser.getUsername(), "g")
        self.assertEqual(testUser.getHighScore(), 0)

        usersList = User.objects.all().values_list("Username", flat=True)
        print(usersList)
        self.assertEqual(usersList.__len__(), 1)
